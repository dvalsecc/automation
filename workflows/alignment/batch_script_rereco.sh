#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Execute the actual processing here, this can be a CMSSW job or anything else (cmsRun is just an example)
mkdir output

export PYTHON3PATH=$PYTHON3PATH:$CMSSW_BASE/src/EcalValidation/EcalAlignment/test
cmsDriver.py reco -s RAW2DIGI,L1Reco,RECO,PAT \ 
            --conditions ${GT} --era Run3 \ 
            --data --python_filename=align_rereco_cfg.py --no_exec \ 
            --processName=EcalAlignment \ 
            --customise align_rereco_customise.py \ 
            --filein=$INFILE 

cmsRun align_rereco_cfg.py
RETCMSSW=$?

if [ "$RETCMSSW" == "0" ]
then
    # move the file to the final location (example)
    mkdir -p $EOSDIR
    cp output/output.root $EOSDIR/ecal_alignment_rereco_$JOBID.root
    rm *root
    OFILE=$EOSDIR/ecal_alignment_rereco_$JOBID.root
    RETCOPY=$?
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
