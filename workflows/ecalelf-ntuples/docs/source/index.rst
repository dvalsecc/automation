Automation workflow docs: ECALElf ntuples production
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

run-wskim.py
============

.. argparse::
   :filename: ../run-wskim.py
   :func: get_opts
   :prog: run-wskim.py

run-zskim.py
============

.. argparse::
   :filename: ../run-zskim.py
   :func: get_opts
   :prog: run-zskim.py
