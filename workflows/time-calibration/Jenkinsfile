@Library("ectrl-shared-libs")
import static ectrl.vars.GlobalVars.*
setenv(this, env.JOB_BASE_NAME)

pipeline {
    agent {
        label 'lxplus'
    }
    stages {
        stage('reco-check') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 runreco.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing/ check --max-retries=10
                    python3 runreco_cc.py --notify=$notify_url  --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing_cc/ check --max-retries=10
                    """
                }
            }
        }
        stage('reco-resubmit') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 runreco.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing/ resubmit --template template.sub --resub-flv='tomorrow'
                    python3 runreco_cc.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing_cc/ resubmit --template template_cc.sub --resub-flv='tomorrow'
                    """
                }
            }
        }
        stage('reco-submit') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 runreco.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing/ submit --template template.sub --nfiles=1 --t0
                    python3 runreco_cc.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing_cc/ submit --template template_cc.sub --nfiles=1 --t0
                    """
                }
            }
        }
        stage('val-check') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 validation/runval.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ check --max-retries=10
                    python3 validation/runval_cc.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ check --max-retries=10
                    """
                }
            }
        }
        stage('val-resubmit') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 validation/runval.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ resubmit --plotsurl=$plotsurl
                    #python3 validation/runval_cc.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ resubmit --plotsurl=$plotsurl
                    """
                }
            }
        }
        stage('val-submit') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 validation/runval.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ submit --plotsurl=$plotsurl
                    #python3 validation/runval_cc.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ submit --plotsurl=$plotsurl
                    """
                }
            }
        }
        stage('rereco-check') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 runrereco.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing-rereco/ check --max-retries=10
                    python3 runrereco_cc.py --notify=$notify_url  --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing_cc-rereco/ check --max-retries=10
                    """
                }
            }
        }
        stage('rereco-resubmit') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 runrereco.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing-rereco/ resubmit --template template_rereco.sub --resub-flv='tomorrow'
                    #python3 runrereco_cc.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing_cc-rereco/ resubmit --template template_rereco_cc.sub --resub-flv='tomorrow'
                    """
                }
            }
        }
        stage('rereco-submit') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 runrereco.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing-rereco/ submit --template template_rereco.sub --nfiles=1 --t0
                    #python3 runrereco_cc.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/timing_cc-rereco/ submit --template template_rereco_cc.sub --nfiles=1 --t0
                    """
                }
            }
        }
        stage('val-rereco-check') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 validation/runval_rereco.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ check --max-retries=10
                    python3 validation/runval_rereco_cc.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ check --max-retries=10
                    """
                }
            }
        }
        stage('val-rereco-resubmit') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 validation/runval_rereco.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ resubmit --plotsurl=$plotsurl
                    #python3 validation/runval_rereco_cc.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ resubmit --plotsurl=$plotsurl
                    """
                }
            }
        }
        stage('val-rereco-submit') {
            steps {
                dir('workflows/time-calibration/') {
                    automationApptainerStep """
                    python3 validation/runval_rereco.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ submit --plotsurl=$plotsurl
                    #python3 validation/runval_rereco_cc.py --notify=$notify_url --campaign \$CAMPAIGN --db=$dbinstance --wdir \$CMSSW_BASE --eosdir $eospath/ submit --plotsurl=$plotsurl
                    """
                }
            }
        }
    }
    post {
        unsuccessful {
            sendMattermostNotification "ERROR"
        }
    }
}

