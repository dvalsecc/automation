# Introduction

This guide is meant to collect all the information related to the framework that 
provide automation of the CMS ECAL calibration and monitoring workflows.

The framework was designed and developed during the LHC LS2 and is currently in its
commissioning phase.

The system has been designed following these principles:

- Rely as much as possible on market software, minimizing the need of ad-hoc code.
- Integrate the existing calibration workflows limiting the amount of code re-writing.
- Provide simple monitoring tools (here monitoring refers to the monitoring of the
  automation, not of the ECAL).
- Support both prompt-reco and re-recos calibration campaigns (ideally without the need to
  adjust the workflow too much when moving from one to the others).
  
## Calibration goals

The main goal of the automation system is to provide a performance of the ECAL (in terms of energy scale stability and resolution) with prompt reconstructed data as close as possible to 
the ultimate performance achieved after the recalibration of the Run2 dataset.

Ultimately this boils down to control the ECAL response variation due to mainly two effects:
- ECAL Barrel (EB): drift of some components of the laser monitoring system (PN and fibers).
- ECAL Endcap (EE): changes to the transparency corrections =alpha= parameter over eta.

